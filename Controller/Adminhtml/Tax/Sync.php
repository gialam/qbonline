<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\QuickBooksOnline\Controller\Adminhtml\Tax;

/**
 * Class Sync
 * @package Magenest\QuickBooksOnline\Controller\Adminhtml\Tax
 */
class Sync extends AbstractTax
{
    /**
     * execute the action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $totals = 0;
        $taxList = $this->taxModel->create()->getCollection();
        foreach ($taxList as $listTax) {
            try {
                $this->taxCode->sync($listTax->getTaxId(), trim($listTax->getTaxCode()), $listTax->getRate());
                $totals++;
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        }
        $this->messageManager->addSuccessMessage(
            __(
                sprintf('Totals %s Tax Code have been sync/update to QuickBooksOnline.', $totals)
            )
        );
        $this->_redirect('*/*/index');
    }
}
