<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_QuickBooksOnline extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_QuickBooksOnline
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\QuickBooksOnline\Controller\Adminhtml\Connection;

use Magenest\QuickBooksOnline\Controller\Adminhtml\AbstractConnection;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Begin
 *
 * @package Magenest\QuickBooksOnline\Controller\Adminhtml\Connection
 */
class Begin extends AbstractConnection
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $websiteId = $this->getRequest()->getParam('website');
        $qboMode = $this->getRequest()->getParam('qbo_mode');
        $callbackUrl = $this->getUrl(
            '*/*/success',
            [
                'website' => $websiteId,
                'qbo_mode' => $qboMode
            ]
        );

        /** @var \Magento\Framework\Controller\Result\Redirect $redirectPage */
        $redirectPage = $this->resultFactory->create('redirect');
        $redirectPage->setPath('/');

        try {
            $redirectUrl = $this->authenticate->redirectUrl($callbackUrl);
            $this->_redirect($redirectUrl);
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $redirectPage;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $redirectPage;
        }
    }
}
