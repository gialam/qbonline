<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\QuickBooksOnline\Controller\Adminhtml\Queue;

use Magenest\QuickBooksOnline\Controller\Adminhtml\AbstractQueue;
use Magenest\QuickBooksOnline\Helper\Synchronization;

/**
 * Class Sync
 * @package Magenest\QuickBooksOnline\Controller\Adminhtml\Queue
 */
class Sync extends AbstractQueue
{
    /**
     * Execute the action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var Synchronization $cronjob */
        $cronJob = $this->_objectManager->create(Synchronization::class);
        $cronJob->execute();

        $this->_redirect('*/*/index');
    }
}
