<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_QuickBooksOnline extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_QuickBooksOnline
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\QuickBooksOnline\Controller\Adminhtml\Payment\Methods;

use Magenest\QuickBooksOnline\Controller\Adminhtml\Payment\AbstractPaymentMethods;

/**
 * Class Sync
 *
 * @package Magenest\QuickBooksOnline\Controller\Adminhtml\Payment\Methods
 */
class Sync extends AbstractPaymentMethods
{
    /**
     * execute the action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $totals = 0;
        $paymentMethodsList = $this->_scopeConfig->getValue('payment');
        foreach ($paymentMethodsList as $code => $data) {
            if (isset($data['active']) && isset($data['title'])) {
                $title = $data['title'];
                if (strlen($title) > 31) {
                    $title = substr($title, 0, 31);
                    $this->messageManager->addNoticeMessage(
                        __(
                            sprintf(
                                'Payment Methods \'%s\' renamed to \'%s\' when synced to QuickBooks Online',
                                $data['title'],
                                $title
                            )
                        )
                    );
                }
                try {
                    $this->paymentMethods->sync($title, $code);
                    $totals++;
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                }
            }
        }
        
        $this->messageManager->addSuccessMessage(
            __(
                sprintf('Totals %s Payment Methods have been sync/update to QuickBooksOnline.', $totals)
            )
        );
        $this->_redirect('*/*/index');
    }
}
