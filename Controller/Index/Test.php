<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_ProductLabel extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_ProductLabel
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\QuickBooksOnline\Controller\Index;

use Magento\Sales\Model\Order\Creditmemo as CreditmemoModel;
use Magento\Sales\Model\Order\Invoice as InvoiceModel;

/**
 * Class Index
 *
 * @package Magenest\ProductLabel\Controller\Adminhtml\Label
 */
class Test extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $encryptor;
    protected $_productRepository;
    protected $ticketFactory;
    protected $quote;
    protected $item;
    protected $orderFactory;

    protected $_creditmemo;

    protected $_invoice;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Encryption\Encryptor $encryptor,
        \Magento\Catalog\Model\ProductFactory $productRepository,
        \Magento\Quote\Model\Quote\ItemFactory $itemFactory,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        CreditmemoModel $creditmemo,
        InvoiceModel $invoice
    ) {
        $this->orderFactory = $orderFactory;
        $this->quote = $quote;
        $this->item = $itemFactory;
        $this->_productRepository = $productRepository;
        $this->encryptor = $encryptor;
        $this->resultPageFactory = $resultPageFactory;
        $this->_creditmemo  = $creditmemo;
        $this->_invoice  = $invoice;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return void
     */
    public function execute()
    {
        $i = 222;
        $b = round($i/1000);
        print_r($b);
    }
}
