<?php
namespace Magenest\QuickBooksOnline\Model;

use Magenest\QuickBooksOnline\Helper\Oauth as OauthHelper;
use Magento\Framework\Exception\LocalizedException;
use Magenest\QuickBooksOnline\Model\OauthFactory as OauthModelFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Config\Model\Config as ConfigModel;

/**
 * Class Authenticate
 * @package Magenest\QuickBooksOnline\Model
 */
class Authenticate
{

    /**@#+
     * Constants
     */
    const URL_CONNECT_BEGIN = 'https://appcenter.intuit.com/Connect/Begin';
    const URL_REQUEST_TOKEN = 'https://oauth.intuit.com/oauth/v1/get_request_token';
    const URL_ACCESS_TOKEN  = 'https://oauth.intuit.com/oauth/v1/get_access_token';

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var OauthHelper
     */
    protected $_oauthHelper;

    /**
     * @var OauthModelFactory
     */
    protected $oauthModelFactory;

    /**
     * @var ConfigModel
     */
    protected $configModel;

    /**
     * @var Client
     */
    protected $client;

    /**
     * Authenticate constructor.
     *
     * @param RequestInterface $request
     * @param OauthHelper $oauthHelper
     * @param OauthFactory $oauthModelFactory
     * @param ConfigModel $configModel
     * @param Client $client
     */
    public function __construct(
        RequestInterface $request,
        OauthHelper $oauthHelper,
        OauthModelFactory $oauthModelFactory,
        ConfigModel $configModel,
        Client $client
    ) {
        $this->_request = $request;
        $this->_oauthHelper = $oauthHelper;
        $this->oauthModelFactory = $oauthModelFactory;
        $this->configModel = $configModel;
        $this->client = $client;
    }

    /**
     * Oauth Helper
     *
     * @return OauthHelper
     */
    public function getHelper()
    {
        return $this->_oauthHelper;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getParam($key = '')
    {
        return $this->_request->getParam($key);
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->_request->getParams();
    }

    /**
     * Authenticate Url
     *
     * @param $callBackUrl
     * @return string
     * @throws LocalizedException
     */
    public function redirectUrl($callBackUrl)
    {
        $response = $this->client->request(
            self::URL_REQUEST_TOKEN,
            ['oauth_callback' => $callBackUrl]
        );

        parse_str($response, $oauth);

        if (is_array($oauth)
            && !empty($oauth['oauth_token'])
            && !empty($oauth['oauth_token_secret'])
        ) {
            $this->saveRequestTokenToDb($oauth);

            $desUrl = self::URL_CONNECT_BEGIN
                . '?oauth_callback=' . urlencode($callBackUrl)
                . '&oauth_consumer_key=' . $this->getHelper()->getConsumerKey()
                . '&oauth_token=' . $oauth['oauth_token'];

            return $desUrl;
        } else {
            throw new LocalizedException(
                __('Can\'t get the request token in QuickBooks Online')
            );
        }
    }
    
    /**
     * @param array $oauth
     */
    private function saveRequestTokenToDb($oauth)
    {
        $model = $this->oauthModelFactory->create();
        $model->getCurrentConnection();
        
        $data = [
            'app_username'               => $this->getHelper()->getApplicationToken(),
            'oauth_request_token'        => $oauth['oauth_token'],
            'oauth_request_token_secret' => $oauth['oauth_token_secret']
        ];
        $model->saveRequestToken($data);
    }

    /**
     * Retrieve Access Token
     *
     * @return bool
     * @throws LocalizedException
     * @throws \Zend_Http_Client_Exception
     */
    public function retrieveAccessToken()
    {
        $model = $this->oauthModelFactory->create();
        $model->getCurrentConnection();

        $params = $this->prepareParamsForRetrieveAccessToken($model);
        $response = $this->client->request(self::URL_ACCESS_TOKEN, $params);
        parse_str($response, $oauth);

        if (is_array($oauth)
            && !empty($oauth['oauth_token'])
            && !empty($oauth['oauth_token_secret'])
        ) {
            try {
                $params = $this->getParams();
                $data = [
                    'oauth_access_token'        => $oauth['oauth_token'],
                    'oauth_access_token_secret' => $oauth['oauth_token_secret'],
                    'qb_realm'                  => $params['realmId'],
                    'qb_flavor'                 => $params['dataSource'],
                ];

                $model->saveAccessToken($data);
                $this->saveToConfigTable($params);
            } catch (\Exception $e) {
                throw new LocalizedException(
                    __('Have an error when saved the access token: ' . $e->getMessage())
                );
            }
        } else {
            throw new LocalizedException(
                __('We can\'t receive the access token. Please try to connect again!')
            );
        }
    }

    /**
     * Prepare Params
     *
     * @param Oauth $model
     *
     * @return array
     * @throws LocalizedException
     */
    protected function prepareParamsForRetrieveAccessToken($model)
    {
        if (!$model->getId()) {
            throw new LocalizedException(
                __('We can\'t fetch the request token from the database!')
            );
        }
        $oauthVerifier = $this->getParam('oauth_verifier');

        if (!$oauthVerifier) {
            throw new LocalizedException(
                __('We can\'t receive Oauth Verifier Token from QuickBooks Online')
            );
        }

        $params = [
            'oauth_token'    => $model->getOauthRequestToken(),
            'oauth_secret'   => $model->getOauthRequestTokenSecret(),
            'oauth_verifier' => $oauthVerifier,
        ];

        return $params;
    }

    /**
     * Save to `core_config_data` table
     *
     * @param $params
     */
    protected function saveToConfigTable($params)
    {
        $qboMode = $this->getParam('qbo_mode');
        
        $this->saveDataByPath(Config::XML_PATH_QBONLINE_IS_CONNECTED, 1);
        $this->saveDataByPath(Config::XML_PATH_QBONLINE_APP_MODE, $qboMode);
        $this->saveDataByPath(Config::XML_PATH_QBONLINE_COMPANY_ID, $params['realmId']);
    }

    /**
     * @param $path
     * @param $value
     * @return $this
     * @throws \Exception
     */
    private function saveDataByPath($path, $value)
    {
        $this->configModel->setDataByPath($path, $value);
        $this->configModel->save();

        return $this;
    }
}
