<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_QuickBooksOnline extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_QuickBooksOnline
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\QuickBooksOnline\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class PaymentMethod
 *
 * @package Magenest\QuickBooksOnline\Model
 * @method int getSyncToken()
 * @method int getQboId()
 * @method string getTitle()
 * @method PaymentMethods setTitle(string $title)
 * @method PaymentMethods setPaymentCode(string $code)
 * @method PaymentMethods setQboId(int $id)
 * @method PaymentMethods setSyncToken(int $syncToken)
 */
class PaymentMethods extends AbstractModel
{
    /**
     * Initize
     */
    public function _construct()
    {
        $this->_init('Magenest\QuickBooksOnline\Model\ResourceModel\PaymentMethods');
    }

    /**
     * Load By Category Id
     *
     * @param $id
     * @return $this
     */
    public function loadByCode($id)
    {
        return $this->load($id, 'payment_code');
    }
}
