<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_QuickBooksOnline extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_QuickBooksOnline
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\QuickBooksOnline\Model\Synchronization;

use Magenest\QuickBooksOnline\Model\Client;
use Magenest\QuickBooksOnline\Model\Log;
use Magenest\QuickBooksOnline\Model\Synchronization;
use Magento\Sales\Model\Order\Invoice as InvoiceModel;
use Magento\Framework\Exception\LocalizedException;
use Magenest\QuickBooksOnline\Model\TaxFactory;
use Magenest\QuickBooksOnline\Model\Config;
use Magento\Sales\Model\OrderFactory;

/**
 * Class Invoice using to sync Invoice
 *
 * @package Magenest\QuickBooksOnline\Model\Sync
 * @method InvoiceModel getModel()
 */
class Invoice extends Synchronization
{
    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var Customer
     */
    protected $_syncCustomer;

    /**
     * @var Item
     */
    protected $_item;

    /**
     * @var InvoiceModel
     */
    protected $_invoice;

    /**
     * @var InvoiceModel
     */
    protected $_currentModel;

    /**
     * @var TaxFactory
     */
    protected $tax;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var PaymentMethods
     */
    protected $_paymentMethods;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * @var InvoiceModel\ItemFactory
     */
    protected $itemInvoice;

    /**
     * Invoice constructor.
     * @param Client $client
     * @param Log $log
     * @param InvoiceModel $invoice
     * @param Item $item
     * @param Customer $customer
     * @param TaxFactory $taxFactory
     * @param Config $config
     * @param \Magenest\QuickBooksOnline\Model\PaymentMethodsFactory $paymentMethods
     * @param \Psr\Log\LoggerInterface $logger
     * @param OrderFactory $orderFactory
     */
    public function __construct(
        Client $client,
        Log $log,
        InvoiceModel $invoice,
        Item $item,
        Customer $customer,
        TaxFactory $taxFactory,
        Config $config,
        \Magenest\QuickBooksOnline\Model\PaymentMethodsFactory $paymentMethods,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Sales\Model\Order\Invoice\ItemFactory $invoiceItemFactory,
        \Psr\Log\LoggerInterface $logger,
        OrderFactory $orderFactory
    ) {
        parent::__construct($client, $log);
        $this->_invoice  = $invoice;
        $this->_item     = $item;
        $this->_syncCustomer = $customer;
        $this->tax = $taxFactory;
        $this->type = 'invoice';
        $this->config = $config;
        $this->_paymentMethods = $paymentMethods;
        $this->_orderFactory = $orderFactory;
        $this->logger = $logger;
        $this->product = $product;
        $this->itemInvoice = $invoiceItemFactory;
    }

    /**
     * Sync Invoice
     *
     * @param $incrementId
     * @return mixed
     * @throws LocalizedException
     */
    public function sync($incrementId)
    {
        $model = $this->_invoice->loadByIncrementId($incrementId);
        $checkInvoice = $this->checkInvoice($incrementId);
        if (isset($checkInvoice['Id'])) {
            $this->addLog($incrementId, $checkInvoice['Id'], 'This Invoice have existed .If you want resync , you need to change prefix invoice on configuration.');
        } else {
            try {
                if (!$model->getId()) {
                    throw new LocalizedException(__('We can\'t find the Invoice #%1', $incrementId));
                }
                $this->setModel($model);
                $this->prepareParams();
                $params = $this->getParameter();
                $response = $this->sendRequest(\Zend_Http_Client::POST, 'invoice', $params);
                $qboId = $response['Invoice']['Id'];
                $this->addLog($incrementId, $qboId);
                $this->parameter = [];
                return $qboId;
            } catch (LocalizedException $e) {
                $this->addLog($incrementId, null, $e->getMessage());
            }
        }

        $this->parameter = [];
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    protected function prepareParams()
    {
        $model = $this->getModel();
        $prefix = $this->config->getPrefix('invoice');
        $params = [
            'DocNumber'    => $prefix.$model->getIncrementId(),
            'TxnDate'      => $model->getCreatedAt(),
            'TxnTaxDetail' => ['TotalTax' => $model->getTaxAmount()],
            'CustomerRef'  => $this->prepareCustomerId(),
            'Line'         => $this->prepareLineItems(),
            'TotalAmt'     => $model->getGrandTotal(),
            'BillEmail'    => ['Address' => $model->getOrder()->getCustomerEmail()],
        ];

        $this->setParameter($params);
        // st Tax
        if ($this->config->getCountry() != 'UK' && $model->getTaxAmount() > 0) {
            $this->prepareTax();
        }
        $this->prepareBillingAddress();
        $this->prepareShippingAddress();

        return $this;
    }

    /**
     * Create Tax
     */
    public function prepareTax()
    {
        $params['TxnTaxDetail'] = [
            'TotalTax' => $this->getModel()->getTaxAmount(),
        ];
    }


    /**
     * @return array
     * @throws LocalizedException
     */
    public function prepareCustomerId()
    {
        try {
            $model = $this->getModel();
            $customerId = $model->getOrder()->getCustomerId();
            if ($customerId) {
                $cusRef = $this->_syncCustomer->sync($customerId);
            } else {
                $cusRef = $this->_syncCustomer->syncGuest(
                    $model->getBillingAddress(),
                    $model->getShippingAddress()
                );
            }

            return ['value' => $cusRef];
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Can\'t sync customer on Invoice to QBO')
            );
        }
    }

    /**
     * @return void
     */
    public function prepareBillingAddress()
    {
        $billAddress = $this->getModel()->getBillingAddress();
        if ($billAddress !== null) {
            $params['BillAddr'] = $this->getAddress($billAddress);
            $this->setParameter($params);
        }
    }

    /**
     * @return void
     */
    public function prepareShippingAddress()
    {
        $shippingAddress = $this->getModel()->getShippingAddress();
        if ($shippingAddress !== null) {
            $params['ShipAddr'] = $this->getAddress($shippingAddress);
            $this->setParameter($params);
        }
    }

    /**
     * @return bool|int
     */
    public function getTaxFree()
    {
        $modelTax = $this->tax->create()->load('tax_uk_zero', 'tax_code');
        if ($modelTax) {
            return $modelTax->getQboId();
        }

        return false;
    }

    /**
     * Create Tax
     */
    public function prepareTaxCodeRef($itemId)
    {
        $taxCode = 0 ;
        /** @var \Magento\Sales\Model\Order\Tax\Item $modelTaxItem */
        $modelTaxItem = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Sales\Model\Order\Tax\Item')->load($itemId, 'item_id');
        if ($modelTaxItem) {
            $taxId = $modelTaxItem->getTaxId();
            $modelTax = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Sales\Model\Order\Tax')->load($taxId);

            if ($modelTax && !empty($modelTax->getData())) {
                $taxCode = $modelTax->getCode();
            }
            $tax = $this->tax->create()->load($taxCode, 'tax_code');
            if ($tax->getQboId() && $tax->getQboId() > 0) {
                $taxCodeId = $tax->getQboId();

                return $taxCodeId;
            }
        }

        return false;
    }

    /**
     * Add Item to Order
     *
     * @return array
     */
    public function prepareLineItems()
    {
        $i     = 1;
        $lines = [];
        foreach ($this->getModel()->getAllItems() as $item) {
            /** @var \Magento\Catalog\Model\Product $modelProduct */
            $modelProduct = $this->product->create()->load($item->getProductId());
            $productType = $modelProduct->getTypeId();
            if (isset($productType) && $productType != null) {
                if ($productType == "configurable") {
                    foreach ($item->getOrderItem()->getChildrenItems() as $test) {
                        $productId = $test->getProductId();
                        $price     = $item->getPrice();
                        $qty     = $test->getQtyOrdered();
                        $total   = $item->getRowTotal();
                        $itemId  = $this->_item->sync($productId);
                        $tax = $item->getTaxAmount() > 0 ? true : false;
                    }
                } else {
                    $productId = $item->getProductId();
                    $price     = $item->getPrice();
                    $qty     = $item->getQty();
                    $total   = $item->getRowTotal() ? $item->getRowTotal() : '0.0000';
                    $tax = $item->getTaxAmount() > 0 ? true : false;
                    $itemId  = $this->_item->sync($productId);
                }
                if ($this->config->getCountry() != 'UK') {
                    $lines[] = [
                        'LineNum'             => $i,
                        'Amount'              => $total,
                        'DetailType'          => 'SalesItemLineDetail',
                        'SalesItemLineDetail' => [
                            'ItemRef'    => ['value' => $itemId],
                            'UnitPrice'  => $price,
                            'Qty'        => $qty,
                            'TaxCodeRef' => ['value' => $tax ? 'TAX' : 'NON'],
                        ],
                    ];
                } else {
                    $lines[] = [
                        'LineNum'             => $i,
                        'Amount'              => $total,
                        'DetailType'          => 'SalesItemLineDetail',
                        'SalesItemLineDetail' => [
                            'ItemRef'    => ['value' => $itemId],
                            'UnitPrice'  => $price,
                            'Qty'        => $qty,
                            'TaxCodeRef' => ['value' => $tax ? $this->prepareTaxCodeRef($item->getOrderItemId()) : $this->getTaxFree()]
                        ],
                    ];
                }
            }

            $i++;
        }

        //TODO
        $lines[] = $this->prepareLineShippingFee();
        $lines[] = $this->prepareLineDiscountAmount();

        return $lines;
    }

    /**
     * @param bool $hasTax
     * @return array
     */
    public function getTaxCodeRef($hasTax)
    {
        return ['value' => $hasTax ? 'TAX' : 'NON'];
    }

    /**
     * @return array
     */
    public function prepareLineShippingFee()
    {
        $shippingAmount = $this->getModel()->getShippingInclTax();

        if ($this->config->getCountry() == 'UK') {
            $lines = [
                'Amount'              => $shippingAmount ? $shippingAmount : 0,
                'DetailType'          => 'SalesItemLineDetail',
                'SalesItemLineDetail' => [
                    'ItemRef'    => ['value' => 'SHIPPING_ITEM_ID'],
                    'TaxCodeRef' => ['value' => $this->config->getTaxShipping()],
                ],
            ];
        } else {
            $lines = [
                'Amount'              => $shippingAmount ? $shippingAmount : 0,
                'DetailType'          => 'SalesItemLineDetail',
                'SalesItemLineDetail' => [
                    'ItemRef'    => ['value' => 'SHIPPING_ITEM_ID'],
                ],
            ];
        }

        return $lines;
    }

    /**
     * @return array
     */
    public function prepareLineDiscountAmount()
    {
        $discountAmount = $this->getModel()->getDiscountAmount();
        $lines = [
            'Amount'             => $discountAmount ?  -1 * $discountAmount : 0,
            'DetailType'         => 'DiscountLineDetail',
            'DiscountLineDetail' => [
                'PercentBased'       => false,
            ]
        ];

        return $lines;
    }

    /**
     * Check invoice by Increment Id
     *
     * @param $id
     * @return array
     */
    protected function checkInvoice($id)
    {
        $prefix = $this->config->getPrefix('invoice');
        $name = $prefix.$id;
        $query = "SELECT Id, SyncToken FROM invoice WHERE DocNumber='{$name}'";

        return $this->query($query);
    }
}
