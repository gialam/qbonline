<?php
/**
 * Created by PhpStorm.
 * User: gialam
 * Date: 16/03/2017
 * Time: 10:11
 */
namespace Magenest\QuickBooksOnline\Block\Adminhtml\Update;

use Magento\Backend\Block\Template;

/**
 * Class UpdateProduct
 * @package Magenest\QuickBooksOnline\Block\Adminhtml
 */
class UpdateProduct extends Template
{
    /**
     * @var string
     */
    protected $_template = 'update/product.phtml';

    /**
     * Update constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getInformationProduct()
    {
        return $this->getUrl('qbonline/update_product/update');
    }

    public function saveProduct()
    {
        return $this->getUrl('qbonline/update_product/save');
    }
}
