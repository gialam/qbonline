<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_QuickBooksOnline extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_QuickBooksOnline
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\QuickBooksOnline\Observer\SalesReceipt;

use Magenest\QuickBooksOnline\Observer\AbstractObserver;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface as ObserverInterface;
use Magenest\QuickBooksOnline\Model\Synchronization\SalesReceipt;
use Magento\Framework\Message\ManagerInterface;
use Magenest\QuickBooksOnline\Model\Config;
use Magenest\QuickBooksOnline\Model\QueueFactory;
use Magento\Sales\Model\OrderFactory;

/**
 * Class Create
 */
class Create extends AbstractObserver implements ObserverInterface
{
    /**
     * @var SalesReceipt
     */
    protected $_salesReceipt;

    /**
     * @var OrderFactory
     */
    protected $order;

    /**
     * Create constructor.
     * @param ManagerInterface $messageManager
     * @param Config $config
     * @param QueueFactory $queueFactory
     * @param SalesReceipt $salesReceipt
     * @param OrderFactory $orderFactory
     */
    public function __construct(
        ManagerInterface $messageManager,
        Config $config,
        QueueFactory $queueFactory,
        SalesReceipt $salesReceipt,
        OrderFactory $orderFactory
    ) {
        parent::__construct($messageManager, $config, $queueFactory);
        $this->_salesReceipt = $salesReceipt;
        $this->order = $orderFactory;
        $this->type = 'salesreceipt';
    }

    /**
     * Dispatch when Invoice created
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            /** @var \Magento\Sales\Model\Order $order */
            $orderIds = $observer->getEvent()->getOrderIds();
            $model = $this->order->create()->load($orderIds[0]);
            $incrementId = $model->getIncrementId();
            if ($incrementId && $this->isEnabled()) {
                if ($this->isImmediatelyMode()) {
                    $this->_salesReceipt->sync($incrementId);
                } else {
                    $this->addToQueue($incrementId);
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
    }
}
