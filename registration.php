<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_ZohoCrm extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_ZohoCrm
 * @author   ThaoPV
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Magenest_QuickBooksOnline',
    __DIR__
);
